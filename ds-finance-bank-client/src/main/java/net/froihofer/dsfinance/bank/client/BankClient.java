package net.froihofer.dsfinance.bank.client;


import java.util.ArrayList;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JFrame;

import net.froihofer.dsfinance.bank.common.BankService;
import net.froihofer.dsfinance.bank.common.ui.entities.Customer;
import net.froihofer.util.JBoss7JndiLookupHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.JTabbedPane;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

public class BankClient {
	
	private Logger log = LoggerFactory.getLogger(BankClient.class);
	private BankService bank;
	private JFrame frame;
	private JTextField txtCustomerId;
	private JTextField txtCustomerName;
	private JList<Customer> customerList;

	public static void main(String[] args) {		
							
		new BankClient();					
		
	}

	public BankClient() {
		
		Properties props = new Properties();
	    // props.put(Context.SECURITY_AUTHENTICATION, "simple");
	    props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
	    props.put(Context.SECURITY_PRINCIPAL, "ejb");
	    props.put(Context.SECURITY_CREDENTIALS, "enterpriseedition");
	    props.put(Context.PROVIDER_URL, "remote://localhost:4447");
	    props.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
	    
	    props.put("jboss.naming.client.ejb.context", true);
	    
	    try {
	        
	        InitialContext context = new InitialContext(props);
	        JBoss7JndiLookupHelper jndiHelper = new JBoss7JndiLookupHelper(context,"ds-finance-bank-ear","ds-finance-bank-ejb",""); 
	        bank = (BankService) jndiHelper.lookupUsingJBossEjbClient( "BankBean", BankService.class, true );
	        
	        ArrayList<Customer> customers = bank.getCustomerList();
	        
	        initFrame();
	        this.frame.setVisible(true);
			refreshData();
		} 
	    catch (NamingException e) {
	    	log.error("Failed to initialize InitialContext.", e);
	    }
	    
		
	}

	private void initFrame() {
		frame = new JFrame();
		frame.setTitle( "Comprehensive Bank Client GUI");
		frame.setBounds(100, 100, 1200, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel customers = new JPanel();
		tabbedPane.addTab("Customers", null, customers, null);
		customers.setLayout(null);
		
		JLabel lblCustomerId = new JLabel("Id");
		lblCustomerId.setBounds(10, 9, 46, 14);
		customers.add(lblCustomerId);
		
		txtCustomerId = new JTextField();
		txtCustomerId.setHorizontalAlignment(SwingConstants.CENTER);
		txtCustomerId.setText("auto generated");
		txtCustomerId.setEditable(false);
		txtCustomerId.setBounds(66, 6, 207, 20);
		customers.add(txtCustomerId);
		txtCustomerId.setColumns(10);
		
		JLabel lblCustomerName = new JLabel("Name");
		lblCustomerName.setBounds(10, 34, 46, 14);
		customers.add(lblCustomerName);
		
		txtCustomerName = new JTextField();
		txtCustomerName.setBounds(66, 31, 207, 20);
		customers.add(txtCustomerName);
		txtCustomerName.setColumns(10);
		
		JButton btnAddCustomer = new JButton("add customer");
		btnAddCustomer.setBounds(154, 62, 119, 23);
		customers.add(btnAddCustomer);
		
		customerList = new JList<Customer>();
		customerList.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		customerList.setBounds(66, 100, 207, 400);
		customers.add(customerList);
		
		JButton btnSelectCustomer = new JButton("select customer");
		btnSelectCustomer.setBounds(154, 514, 119, 23);
		customers.add(btnSelectCustomer);
		
		JPanel stocks = new JPanel();
		tabbedPane.addTab("Stocks", null, stocks, null);
	}

	private void refreshData() {
		
		// customerList.setListData( customers );
	}
}
