package net.froihofer.dsfinance.bank.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JFrame;

import net.froihofer.dsfinance.bank.common.BankService;
import net.froihofer.dsfinance.bank.common.ui.entities.Customer;
import net.froihofer.util.JBoss7JndiLookupHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BankClientOld {
	
  private static Logger log = LoggerFactory.getLogger(BankClientOld.class);
  private static JFrame frame; 
  
  public static void main(String[] args) {
	
    Properties props = new Properties();
    // props.put(Context.SECURITY_AUTHENTICATION, "simple");
    props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
    props.put(Context.SECURITY_PRINCIPAL, "ejb");
    props.put(Context.SECURITY_CREDENTIALS, "enterpriseedition");
    props.put(Context.PROVIDER_URL, "remote://localhost:4447");
    props.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
      
    //Tells the JBoss remote-naming project to internally do whatever is necessary to 
    //setup a EJBClientContext, containing a RemotingConnectionEJBReceiver which
    //is created using the same remoting connection that is created by and being
    //used by remote-naming project for its own JNDI communication usage
    //Therefore no separate there is no need to create and authenticat a separate
    //EJBClientContext.
    
    props.put("jboss.naming.client.ejb.context", true);
    
    initFrame();
    
    try {
      
      InitialContext context = new InitialContext(props);
      JBoss7JndiLookupHelper jndiHelper = new JBoss7JndiLookupHelper(context,"ds-finance-bank-ear","ds-finance-bank-ejb",""); 
      BankService bank = (BankService) jndiHelper.lookupUsingJBossEjbClient( "BankBean", BankService.class, true );
      
      log.debug("connection to BankBean established");
      
      // --- begin - add customers - ---
      
      List<Customer> customerList = new ArrayList<Customer>();      
      
      for(int i = 100; i>0; i--) {
    	  try {
    		  customerList.add( bank.createCustomer("Kunde " + i) );
    	  }
    	  catch (Exception e) {
    		  log.error( "Kunde konnte nicht angelegt werden. Siehe Fehlermeldung.", e);
    	  }    	  
      }
      
      for( Customer c : customerList ) {
    	  log.debug( "Kundenname: " + c.getName() + " Id: " + c.getId() );
      }
      
      // --- end - add customers - ---
      
      
      
      
    }
    catch (NamingException e) {
      log.error("Failed to initialize InitialContext.", e);
    }
  }
  
  private static void initFrame() {
	  BankClientOld.frame = new JFrame();
	  
	  frame.setTitle( "Comprehensive Bank Client GUI");
	  frame.setSize(600, 300);
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.setVisible( true );
	  
  }
  
}
