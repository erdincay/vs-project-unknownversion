package net.froihofer.dsfinance.bank.ejb.dao.intf;

import java.util.List;

import net.froihofer.dsfinance.bank.ejb.db.entities.Bank;

public interface BankDao {

	public List<Bank> list();
	public Bank get( String bankName );
	public void remove( Bank entity );
	public Bank edit( Bank entity );
	public Bank persist( Bank entity );	
	
}
