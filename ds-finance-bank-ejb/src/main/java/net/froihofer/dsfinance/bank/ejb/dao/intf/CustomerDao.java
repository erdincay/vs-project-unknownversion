package net.froihofer.dsfinance.bank.ejb.dao.intf;

import java.util.List;

import net.froihofer.dsfinance.bank.ejb.db.entities.Customer;

public interface CustomerDao {

	public List<Customer> list();
	public Customer get( String customerName );
	public void remove( Customer entity );
	public Customer edit( Customer entity );
	public Customer persist( Customer entity );	
	
}
