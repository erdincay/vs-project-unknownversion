package net.froihofer.dsfinance.bank.ejb.dao.intf;

import java.util.List;

import net.froihofer.dsfinance.bank.ejb.db.entities.Stock;

public interface StockDao {
	
	public List<Stock> list();
	public Stock get( String stockName );
	public void remove( Stock entity );
	public Stock edit( Stock entity );
	public Stock persist( Stock entity );	
	
}
