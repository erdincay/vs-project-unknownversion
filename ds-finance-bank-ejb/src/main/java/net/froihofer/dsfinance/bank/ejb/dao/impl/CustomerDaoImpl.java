package net.froihofer.dsfinance.bank.ejb.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.froihofer.dsfinance.bank.ejb.dao.intf.CustomerDao;
import net.froihofer.dsfinance.bank.ejb.db.entities.Customer;

public class CustomerDaoImpl implements CustomerDao {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> list() {
		return (List<Customer>) entityManager.createQuery("SELECT * FROM CUSTOMER").getResultList();
	}

	@Override
	public Customer get(String customerName) {
		return (Customer) entityManager.createQuery("SELECT * FROM CUSTOMER WHERE NAME LIKE :customerName").setParameter("customerName", customerName).getSingleResult();
	}

	@Override
	public void remove(Customer entity) {
		entityManager.remove( entity );
	}

	@Override
	public Customer edit(Customer entity) {
		entityManager.merge( entity );
		return entity;
	}

	@Override
	public Customer persist(Customer entity) {
		entityManager.persist( entity );
		return entity;
	}	
}
