package net.froihofer.dsfinance.bank.ejb.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.froihofer.dsfinance.bank.ejb.dao.intf.StockDao;
import net.froihofer.dsfinance.bank.ejb.db.entities.Stock;

public class StockDaoImpl implements StockDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Stock> list() {
		return (List<Stock>) entityManager.createQuery("SELECT * FROM STOCK").getResultList();
	}

	@Override
	public Stock get(String stockName) {
		return (Stock) entityManager.createQuery("SELECT * FROM STOCK WHERE SYMBOL LIKE :stockName").setParameter("stockName", stockName).getSingleResult();
	}

	@Override
	public void remove(Stock entity) {
		entityManager.remove( entity );
	}

	@Override
	public Stock edit(Stock entity) {
		entityManager.merge( entity );
		return entity;
	}

	@Override
	public Stock persist(Stock entity) {
		entityManager.persist( entity );
		return entity;
	}
}
