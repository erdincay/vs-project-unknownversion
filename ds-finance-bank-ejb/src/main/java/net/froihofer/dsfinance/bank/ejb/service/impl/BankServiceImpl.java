package net.froihofer.dsfinance.bank.ejb.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.froihofer.dsfinance.bank.common.BankService;
import net.froihofer.dsfinance.bank.ejb.dao.intf.CustomerDao;
import net.froihofer.dsfinance.bank.ejb.db.entities.Customer;

@Stateless(name="BankBean")
public class BankServiceImpl implements BankService {

	@Inject CustomerDao customerDao;

	private String name = "BAWAG";	
	
	public String getBankName() {
		return name;
	}

	@Override
	public net.froihofer.dsfinance.bank.common.ui.entities.Customer createCustomer(String name) {
		
		Customer entity = new Customer();
		net.froihofer.dsfinance.bank.common.ui.entities.Customer dto = new net.froihofer.dsfinance.bank.common.ui.entities.Customer();
		entity.setName(name);		
		customerDao.persist(entity);
		
		dto.setId( entity.getId() );
		dto.setName( entity.getName() );
		return dto;
	}

	@Override
	public boolean deleteCustomer(long id) {
		return false;
	}

	@Override
	public boolean deleteCustomer(String name) {
		return false;
	}

	@Override
	public double getBankVolume() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<net.froihofer.dsfinance.bank.common.ui.entities.Customer> getCustomerList() {
		
		List<Customer> customerList = customerDao.list();
		ArrayList<net.froihofer.dsfinance.bank.common.ui.entities.Customer> items = new ArrayList<net.froihofer.dsfinance.bank.common.ui.entities.Customer>();
		
		for( Customer entity : customerList ) {
			net.froihofer.dsfinance.bank.common.ui.entities.Customer item = new net.froihofer.dsfinance.bank.common.ui.entities.Customer();
			
			item.setId( entity.getId() );
			item.setName( entity.getName() );
			items.add( item );
		}
		return items;
	}
}