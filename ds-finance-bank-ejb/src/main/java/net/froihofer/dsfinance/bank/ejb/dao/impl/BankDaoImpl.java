package net.froihofer.dsfinance.bank.ejb.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.froihofer.dsfinance.bank.ejb.dao.intf.BankDao;
import net.froihofer.dsfinance.bank.ejb.db.entities.Bank;

public class BankDaoImpl implements BankDao {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Bank> list() {
		return (List<Bank>) entityManager.createNamedQuery("SELECT * FROM BANK").getResultList();
	}

	@Override
	public Bank get(String bankName) {
		return (Bank) entityManager.createQuery("SELECT * FROM BANK WHERE NAME LIKE :bankName").setParameter("bankName", bankName).getSingleResult();
	}

	@Override
	public void remove(Bank entity) {
		entityManager.remove( entity );		
	}

	@Override
	public Bank edit(Bank entity) {
		entityManager.merge( entity );
		return entity;
	}

	@Override
	public Bank persist(Bank entity) {
		entityManager.persist( entity );
		return entity;
	}	
}