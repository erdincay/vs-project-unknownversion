package net.froihofer.dsfinance.bank.ejb.db.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BANK")
public class Bank {

	private String BIC; // SURROGATE KEY
	private String name;
	private BigDecimal volume;
	
	public Bank() { }
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="BIC", nullable = false)
	public String getBIC() {
		return this.BIC;
	}
	protected void setBIC(String BIC) {
		this.BIC = BIC;
	}
	
	@Column(name="NAME", nullable = false, unique = true)
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="VOLUME", nullable = false)
	public BigDecimal getVolume() {
		return this.volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}	
}
