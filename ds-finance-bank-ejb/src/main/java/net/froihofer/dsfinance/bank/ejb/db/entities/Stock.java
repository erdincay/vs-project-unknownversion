package net.froihofer.dsfinance.bank.ejb.db.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STOCK")
public class Stock {
	
	private String symbol;
	private BigDecimal price;
	private String company;
	
	public Stock() { }

	@Id
	@Column(name = "SYMBOL", nullable = false)
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	@Column(name="PRICE", nullable = false)
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Column(name="COMPANY", nullable = false)
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}	
}
