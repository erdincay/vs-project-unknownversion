package net.froihofer.dsfinance.bank.common.ui.entities;

import java.io.Serializable;
import java.math.BigDecimal;

public class Bank implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String BIC;
	private String name;	
	private BigDecimal volume;
	
	public Bank() { }
	
	public String getBIC() {
		return this.BIC;
	}	
	public void setBIC(String BIC) {
		this.BIC = BIC;
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getVolume() {
		return this.volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
}