package net.froihofer.dsfinance.bank.common;

import java.util.ArrayList;

import javax.ejb.Remote;

import net.froihofer.dsfinance.bank.common.ui.entities.Customer;

@Remote
public interface BankService {

	// Bank -------------------------------------------------------------------
	public String getBankName();
	public double getBankVolume();

    // Customer ---------------------------------------------------------------
    public boolean deleteCustomer( long id );    
    public boolean deleteCustomer( String name );    
    public Customer createCustomer(String name);
    public ArrayList<Customer> getCustomerList();
    
//    // Bank -----------------------------------------------------------------
//    public DTOCustomer getCustomer( long id );
//    public DTOCustomer searchCustomer( String surName );
//    public List<DTOCustomer> getAllCustomers();
//    -------------------------------------------------------------------------

//    // Stock ----------------------------------------------------------------
//    public DTOStock getStock( long id );
//    public DTOStock searchOneStock( String symbol );
//    public List<DTOStock> searchMultipleStocks( String company );
//    public List<DTOStock> searchMultipleStocks( double value );
//    public List<DTOStock> getAllStocks();    
//    public boolean buyStock( DTOCustomer customer, DTOStock stock, long amount );
//    -------------------------------------------------------------------------

//    // Other ----------------------------------------------------------------
//    void newStock(long id, String symbol, double value, String company);
//    boolean buyStock(long id, String symbol, double value, String company);
//	  boolean sellStock(long id, String symbol, double value, String company);
//    -------------------------------------------------------------------------

}
