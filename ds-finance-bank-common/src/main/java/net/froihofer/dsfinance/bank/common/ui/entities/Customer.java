package net.froihofer.dsfinance.bank.common.ui.entities;

import java.io.Serializable;

public class Customer implements Serializable {
	
	public static final long serialVersionUID = 1L;
	
	private Long id; 
	private String name;
	
	public Customer() { }
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
